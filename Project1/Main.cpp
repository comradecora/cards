#include <iostream>
#include <conio.h>

using namespace std;

enum CardSuit {
	Diamonds, Hearts, Spades, Clubs
};

enum CardRank {
	Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

struct Card {
	CardSuit suit;
	CardRank rank;
};

void PrintCard(Card card) {
	std::cout << "The ";
	switch (card.rank) {
	case Two: std::cout << "Two"; break;
	case Three: std::cout << "Three"; break;
	case Four: std::cout << "Four"; break;
	case Five: std::cout << "Five"; break;
	case Six: std::cout << "Six"; break;
	case Seven: std::cout << "Seven"; break;
	case Eight: std::cout << "Eight"; break;
	case Nine: std::cout << "Nine"; break;
	case Ten: std::cout << "Ten"; break;
	case Jack: std::cout << "Jack"; break;
	case Queen: std::cout << "Queen"; break;
	case King: std::cout << "King"; break;
	case Ace: std::cout << "Ace"; break;
	}
	std::cout << "of ";
	switch (card.suit) {
	case Diamonds: std::cout << "Diamonds"; break;
	case Hearts: std::cout << "Hearts"; break;
	case Spades: std::cout << "Spades"; break;
	case Clubs: std::cout << "Clubs"; break;
	}
}



Card HighCard(Card card1, Card card2) {
	if (card1.rank >= card2.rank) return card1;
	return card2;
}


int main() {
	_getch();
	return 0;

}